import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

public class Main {
  private static String HOME = System.getProperty("user.home");

  public static void main(String[] args) {

    Path p = Paths.get(HOME);
    if (Files.exists(p)) {
      System.out.println("Path exist!");
    }
    System.out.println("File is readable: " + Files.isReadable(p));
    System.out.println("File is writable: " + Files.isWritable(p));
    String fileName = "myfile_" + UUID.randomUUID().toString() + ".txt";
    Path path = Paths.get(HOME + "/" + fileName);
    try {
      System.out.println(Files.createFile(path));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
